const form = document.getElementById('form');
const username = document.getElementById('username');
const email = document.getElementById('Email');
const password = document.getElementById('Password');
const password_check = document.getElementById('Password-check');


form.addEventListener('submit', (e) => {
    e.preventDefault();

    checkInputs ();
});

function checkInputs () {
    let usernamevalue = username.value.trim()
    let emailvalue = email.value.trim()
    let passwordvalue = password.value.trim()
    let password_checkvalue = password_check.value.trim()

    if (usernamevalue === '') {
        setErrorFor (username, 'Username cannot be blank');
    } else {
        setSuccessFor (username)
    }

    if (emailvalue === '') {
        setErrorFor (email, 'Email cannot be blank');
       // alert('Please choose an username')
    }
    else if (!IsEmailValid(emailvalue)) {
        setErrorFor(email, 'Email is not valid');
    }
    else {
        setSuccessFor(email)
    }

    if (!IsPasswordValid(passwordvalue)) {
        setErrorFor(password, 'Password must have more than 5 characters');
    }
    else {
        setSuccessFor(password)
    }

    if (!IsPasswordMatched(password_checkvalue, passwordvalue)) {
        setErrorFor(password_check, 'Password do not match or is undefined');
    }
    else {
        setSuccessFor(password_check)
    }
}

function setErrorFor (input,message) {
    const formControl = input.parentElement;
    const small = formControl.querySelector('small')

    small.innerText = message
    formControl.className = 'form-control error'
}

function setSuccessFor (input) {
    const formControl = input.parentElement;
    formControl.className = 'form-control success'

}

function IsEmailValid (email) {
    let validRegex = /^[a-zA-Z0-9.!#$%&'+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)$/;
    if (email.match(validRegex)) {
        return true;
    } else {
       // alert("Invalid email address!");
        return false;
    }
}

function IsPasswordValid (password) {
    if (password.length < 5 || password.length === 0) {
       // alert("The password must have more than 5 characters");
        return false;
    } else {
       // alert("Password accepted");
        return true;
    }
}

function IsPasswordMatched (password, password_check) {
    if (password_check === password && password.length > 0) {
        //alert("Password confirmed");
        return true;
    } else {
        //alert("Password doesn't match or is undefined");
        return false;
    }
}